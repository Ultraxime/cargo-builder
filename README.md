| ![gplv3-or-later](https://www.gnu.org/graphics/gplv3-or-later.png) | [![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit) |
|--------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------|

# Cargo Builder

## Description

The `maximebruno/cargo-builder` docker image provided by this repository, is designed to provide an easy way to build rust programms.
Two tag are available:

- `latest`: build from the main branch, provides a docker image based on the latest (at the time of build) official Rust image. It can be use to build programms for x86_64 architecture running linux or windows with gnu.
- `musl`: build from the alpine branch, provides a docker image based on the latest (at the time of build) alpine official Rust image. It can be use to build programms for x86_64 architecture running linux with musl.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

<!-- Please make sure to update tests as appropriate. -->

### Pre-Commit

The project already contains a pre-commit-config.

## Authors and acknowledgment

Write your pseudo and profile here under the form :

- [@Ultraxime](https://gitlab.com/Ultraxime)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
