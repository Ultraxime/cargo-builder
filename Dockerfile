ARG tag=latest

FROM rust:${tag}

LABEL org.opencontainers.image.authors="ultraxime@yahoo.com"

RUN cargo install cargo-tarpaulin \
    && rustup component add clippy-preview \
    && cargo install cargo-audit \
    && rustup target add x86_64-pc-windows-gnu \
    && rustup target add x86_64-unknown-linux-gnu

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        curl \
        gcc-mingw-w64-x86-64 \
    && rm -rf /var/cache/apt/archives /var/lib/apt/lists/*
